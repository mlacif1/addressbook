﻿using System;
using System.Collections.Generic;

namespace AddressBook.Models
{
    public sealed class AddressDictionary
    {
        private static readonly Lazy<AddressDictionary> lazy = new Lazy<AddressDictionary>(() => new AddressDictionary());

        public static AddressDictionary Instance { get { return lazy.Value; } }

        private AddressDictionary() { }

        public SortedDictionary<string, string> AddrDict { get; set; }

    }
}