﻿using AddressBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace AddressBook
{
    public partial class InsertAddress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CreateInputGroups();

            if (!IsPostBack)
            {
                //create the dictionary which will hold the addresses the first time the page loads
                if (AddressDictionary.Instance.AddrDict == null)
                    AddressDictionary.Instance.AddrDict = new SortedDictionary<string, string>();
            }
            else
            {        
                //read the textbox values only if they are not empty
                //validation for emptyness will occure dynamically on textbox isDirty
                if (Request.Form["txtName" + 0] != "" && Request.Form["txtPhoneNumber" + 0] != "")
                {
                    inputValues.Clear();
                    var spinnerValue = Convert.ToInt16(txtSpinner.Text) - 1;

                    for (int i = 0; i <= spinnerValue; ++i)
                    {
                        var txtName = Request.Form["txtName" + i];
                        var txtPhone = Request.Form["txtPhoneNumber" + i];

                        Tuple<string, string> inputValue = new Tuple<string, string>(txtName, txtPhone);
                        inputValues.Add(inputValue);

                    }
                }

            }
        }

        /// <summary>
        /// Creates the name ans phoneNumber input groups
        /// </summary>
        private void CreateInputGroups()
        {
            var spinnerValue = Convert.ToInt16(txtSpinner.Text) - 1;

            for (int i = 0; i <= spinnerValue; ++i)
            {
                //creating a container DIV which is repetitive
                Panel jumbotronPanel = CreateJumbotronPanel(i);
                aspInputsContainer.Controls.Add(jumbotronPanel);
            }
        }

        //used to store input values from Request.Form array
        private static List<Tuple<string, string>> inputValues = new List<Tuple<string, string>>();

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string validationErrorMsg = "";
                string insertionErrorMsg = "";

                var inputValuesNames = inputValues.Select(item => item.Item1).Distinct().ToList();
                var inputValuesPhoneNumbers = inputValues.Select(item => item.Item2).Distinct().ToList();

                for (int i = 0; i < inputValues.Count; i++)
                {
                    //if one input is not valid then skip the other inputs validation check
                    //and show error message
                    ValidateInputs(ref validationErrorMsg, ref insertionErrorMsg, inputValuesNames, inputValuesPhoneNumbers, i);
                }

                if (validationErrorMsg != "")
                {
                    lblResult.Text = "Addresses could not be inserted!";
                    lblResult.ForeColor = System.Drawing.Color.Red;
                    if (ContainsPendingAdresses(insertionErrorMsg))
                        RemovePendingAddresses(inputValues);
                }
            }

        }

        /// <summary>
        /// Valiate the specific inputs (name and phoneNumber)
        /// </summary>
        /// <param name="validationErrorMsg"></param>
        /// <param name="insertionErrorMsg"></param>
        /// <param name="inputValuesNames"></param>
        /// <param name="inputValuesPhoneNumbers"></param>
        /// <param name="i"></param>
        private void ValidateInputs(ref string validationErrorMsg, ref string insertionErrorMsg, List<string> inputValuesNames, List<string> inputValuesPhoneNumbers, int i)
        {
            if (IsValidName(inputValues[i].Item1))
            {
                if (IsValidPhoneNumber(inputValues[i].Item2))
                {
                    if (ContainsDuplicateName(inputValuesNames, i))
                    {
                        if (ContainsDuplicatePhoneNumber(inputValuesPhoneNumbers, i))
                        {
                            if (validationErrorMsg == "")
                            {
                                AddressDictionary.Instance.AddrDict.Add(inputValues[i].Item1, inputValues[i].Item2);
                                SetLabelSuccess();
                            }
                        }
                        else
                        {
                            insertionErrorMsg = validationErrorMsg = "Phone Number already exists!";
                            SetLabelError("lblPhoneNumberValid" + i, validationErrorMsg);
                        }
                    }
                    else
                    {
                        insertionErrorMsg = validationErrorMsg = "Name already exists!";
                        SetLabelError("lblNameValid" + i, validationErrorMsg);
                    }
                }
                else
                {
                    validationErrorMsg = "Phone Number Should Contain Only Numbers, One '+' and / or One Pair of Brackets!" + "<br/>" + "Should start with '+' or '0'!" + "<br/>" + "Phone Number Cannot Continue With '0' if it Starts With '+'!";
                    SetLabelError("lblPhoneNumberValid" + i, validationErrorMsg);
                }
            }
            else
            {
                validationErrorMsg = "Name should contain only letters and/or blanks!";
                SetLabelError("lblNameValid" + i, validationErrorMsg);
            }
        }

        /// <summary>
        /// Verifies if we inserted and address that could be duplicated
        /// </summary>
        /// <param name="insertionErrorMsg"></param>
        /// <returns></returns>
        private static bool ContainsPendingAdresses(string insertionErrorMsg)
        {
            return insertionErrorMsg != "" && !insertionErrorMsg.Contains("Name already exists!") && !insertionErrorMsg.Contains("Phone Number already exists!");
        }


        /// <summary>
        /// Checks if there are duplicate phoneNumbers between the user inputs
        /// </summary>
        /// <param name="inputValuesPhoneNumbers"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private static bool ContainsDuplicatePhoneNumber(List<string> inputValuesPhoneNumbers, int i)
        {
            return !AddressDictionary.Instance.AddrDict.ContainsValue(inputValues[i].Item2) && inputValuesPhoneNumbers.Count == inputValues.Count;
        }

        /// <summary>
        /// Checks if there are duplicate names between the user inputs
        /// </summary>
        /// <param name="inputValuesNames"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private static bool ContainsDuplicateName(List<string> inputValuesNames, int i)
        {
            return !AddressDictionary.Instance.AddrDict.ContainsKey(inputValues[i].Item1) && inputValuesNames.Count == inputValues.Count;
        }

        /// <summary>
        /// Sets the label as success if insertion occured
        /// </summary>
        private void SetLabelSuccess()
        {
            lblResult.Text = "Address Was Inserted!";
            lblResult.ForeColor = System.Drawing.ColorTranslator.FromHtml("#009933");
        }

        /// <summary>
        /// Sets the specific label error
        /// </summary>
        /// <param name="lblError"></param>
        /// <param name="validationErrorMsg"></param>
        private void SetLabelError(string lblError, string validationErrorMsg)
        {
            Label currentPhoneNumberValidLabel = new Label();
            currentPhoneNumberValidLabel.ID = lblError;
            currentPhoneNumberValidLabel.Text = validationErrorMsg;
            currentPhoneNumberValidLabel.ForeColor = System.Drawing.Color.Red;
            UpdateLabelError(currentPhoneNumberValidLabel);
        }

        /// <summary>
        /// Updates the status label for every input
        /// </summary>
        /// <param name="currentInputValidLabel"></param>
        private void UpdateLabelError(Label currentInputValidLabel)
        {
            int currLocation, whichPanel, whichIndex;
            Panel panel, oldPanel;
            InitializeLabels(currentInputValidLabel, out currLocation, out panel, out oldPanel, out whichPanel, out whichIndex);

            if (currentInputValidLabel.ID.Contains("PhoneNumber"))
            {
                whichPanel = 1;
                whichIndex = aspInputsContainer.Controls.OfType<Panel>().ElementAt(currLocation).Controls.Count;
            }
            else
            {
                whichIndex = whichPanel = 0;
            }

            InterChangeLabels(currentInputValidLabel, currLocation, out panel, out oldPanel, whichPanel, whichIndex);
        }

        /// <summary>
        /// Prepares the dynamic labels for update
        /// </summary>
        /// <param name="currentInputValidLabel"></param>
        /// <param name="currLocation"></param>
        /// <param name="panel"></param>
        /// <param name="oldPanel"></param>
        /// <param name="whichPanel"></param>
        /// <param name="whichIndex"></param>
        private static void InitializeLabels(Label currentInputValidLabel, out int currLocation, out Panel panel, out Panel oldPanel, out int whichPanel, out int whichIndex)
        {
            currLocation = currentInputValidLabel.ID[currentInputValidLabel.ID.Length - 1] - '0';
            panel = new Panel();
            oldPanel = new Panel();
            whichPanel = 0;
            whichIndex = 0;
        }

        /// <summary>
        /// Finds and interchanges the status labels in the dynamic control
        /// </summary>
        /// <param name="currentInputValidLabel"></param>
        /// <param name="currLocation"></param>
        /// <param name="panel"></param>
        /// <param name="oldPanel"></param>
        /// <param name="whichPanel"></param>
        /// <param name="whichIndex"></param>
        private void InterChangeLabels(Label currentInputValidLabel, int currLocation, out Panel panel, out Panel oldPanel, int whichPanel, int whichIndex)
        {
            panel = aspInputsContainer.Controls.OfType<Panel>().ElementAt(currLocation).Controls.OfType<Panel>().ElementAt(whichPanel);
            oldPanel = aspInputsContainer.Controls.OfType<Panel>().ElementAt(currLocation).Controls.OfType<Panel>().ElementAt(whichPanel);

            var lblInputResult = panel.Controls.OfType<Label>().Last();
            panel.Controls.Remove(lblInputResult);
            panel.Controls.Add(currentInputValidLabel);

            aspInputsContainer.Controls.OfType<Panel>().ElementAt(currLocation).Controls.Remove(oldPanel);
            aspInputsContainer.Controls.OfType<Panel>().ElementAt(currLocation).Controls.AddAt(whichIndex - 1, panel);
        }

        /// <summary>
        /// Removes any pending Addresses if there is a validation error
        /// </summary>
        /// <param name="inputValues"></param>
        private void RemovePendingAddresses(List<Tuple<string, string>> inputValues)
        {
            foreach (var address in inputValues)
            {
                //it is enough to remove the names because there is a name-phoneNumber keyValuePair
                AddressDictionary.Instance.AddrDict.Remove(address.Item1);
            }
        }

        /// <summary>
        /// Checks if the given phoneNumber isValid by checking if:
        /// starts with '+' and continues with '0' then it is invalid
        /// Contains only Numbers and valid brackets
        /// </summary>
        /// <param name="txtPhone"></param>
        /// <returns></returns>
        private bool IsValidPhoneNumber(string txtPhone)
        {
            if (txtPhone.Length > 2 && txtPhone[0] == '+' && txtPhone[1] == '0')
            {
                return false;
            }
            else
            {
                if (Regex.IsMatch(txtPhone, "^[0+][0-9()]+$") && BracketsAreValid(txtPhone))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        /// <summary>
        /// Checks if a name only contains letters
        /// </summary>
        /// <param name="txtName"></param>
        /// <returns></returns>
        private bool IsValidName(string txtName)
        {
            if (Regex.IsMatch(txtName, @"^[a-zA-Z ]+$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if there if only one pair of perfectly closed round brackets
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        private bool BracketsAreValid(string phoneNumber)
        {
            var openBracket = phoneNumber.Count(c => c == '(');
            var closeBracket = phoneNumber.Count(c => c == ')');

            if (openBracket == closeBracket)
            {
                if (openBracket == 0)
                    return true;
                else
                {
                    if (openBracket == 1 && phoneNumber.IndexOf('(') < phoneNumber.IndexOf(')'))
                    {
                        return true;
                    }
                    else return false;
                }
            }
            else return false;
        }

        /// <summary>
        /// Creates the repetitive container in which the textbox inputs are located
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private static Panel CreateJumbotronPanel(int count)
        {
            Panel jumnbotronPanel = new Panel();
            jumnbotronPanel.CssClass = "jumbotron theme-showcase";

            Panel controlsPanelName = CreateNameControlsPanel(count);
            Panel controlsPanelPhoneNumber = CreatePhoneNumberControlsPanel(count);

            jumnbotronPanel.Controls.Add(controlsPanelName);
            jumnbotronPanel.Controls.Add(controlsPanelPhoneNumber);
            return jumnbotronPanel;
        }

        /// <summary>
        /// Creates the Container for the phoneNumber input
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private static Panel CreatePhoneNumberControlsPanel(int count)
        {
            Panel controlsPanelPhoneNumber = new Panel();
            controlsPanelPhoneNumber.CssClass = "form-group";

            Label lblPhoneNumber = new Label();
            lblPhoneNumber.ID = "lblPhoneNumber" + count;
            lblPhoneNumber.Text = "Phone Number";
            lblPhoneNumber.CssClass = "label label-primary";

            TextBox txtPhoneNumber = new TextBox();
            txtPhoneNumber.ID = "txtPhoneNumber" + count;
            txtPhoneNumber.CssClass = "form-control";
            txtPhoneNumber.Attributes["placeholder"] = "Phone Number";

            RequiredFieldValidator phoneNumberRequiredValidator = new RequiredFieldValidator();
            phoneNumberRequiredValidator.ID = "phoneNumberRequiredValidator" + count;
            phoneNumberRequiredValidator.Display = ValidatorDisplay.Dynamic;
            phoneNumberRequiredValidator.ErrorMessage = "Phone Number is Required!";
            phoneNumberRequiredValidator.ControlToValidate = "txtPhoneNumber" + count;
            phoneNumberRequiredValidator.ForeColor = System.Drawing.Color.Red;
            phoneNumberRequiredValidator.ValidationGroup = "validGroup";

            Label lblPhoneNumberValid = new Label();
            lblPhoneNumberValid.ID = "lblPhoneNumberValid" + count;

            controlsPanelPhoneNumber.Controls.Add(lblPhoneNumber);
            controlsPanelPhoneNumber.Controls.Add(txtPhoneNumber);
            controlsPanelPhoneNumber.Controls.Add(phoneNumberRequiredValidator);
            controlsPanelPhoneNumber.Controls.Add(lblPhoneNumberValid);

            return controlsPanelPhoneNumber;
        }

        /// <summary>
        /// Creates the container for the name input
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private static Panel CreateNameControlsPanel(int count)
        {
            Panel controlsDivPanel = new Panel();
            controlsDivPanel.CssClass = "form-group";

            Label lblName = new Label();
            lblName.ID = "lblName" + count;
            lblName.Text = "Name";
            lblName.CssClass = "label label-primary";

            TextBox txtName = new TextBox();
            txtName.ID = "txtName" + count;
            txtName.CssClass = "form-control";
            txtName.Attributes["placeholder"] = "Name";

            RequiredFieldValidator nameRequiredValidator = new RequiredFieldValidator();
            nameRequiredValidator.ID = "nameRequiredValidator" + count;
            nameRequiredValidator.Display = ValidatorDisplay.Dynamic;
            nameRequiredValidator.ErrorMessage = "Name is Required!";
            nameRequiredValidator.ControlToValidate = "txtName" + count;
            nameRequiredValidator.ForeColor = System.Drawing.Color.Red;
            nameRequiredValidator.ValidationGroup = "validGroup";

            Label lblNameValid = new Label();
            lblNameValid.ID = "lblNameValid" + count;

            controlsDivPanel.Controls.Add(lblName);
            controlsDivPanel.Controls.Add(txtName);
            controlsDivPanel.Controls.Add(nameRequiredValidator);
            controlsDivPanel.Controls.Add(lblNameValid);

            return controlsDivPanel;
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            aspInputsContainer.Controls.Clear();
            CreateInputGroups();
        }
    }
}