﻿using AddressBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddressBook
{
    public partial class SearchAddress : Page
    {
        private string _phoneNumber = "";
        private string _name = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            _phoneNumber = txtPhoneNumber.Text;
            _name = txtName.Text;
        }

        //We must reload the data inside the gridView from Session when pagination happens
        protected void gridAddressBook_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridAddressBook.PageIndex = e.NewPageIndex;

            gridAddressBook.DataSource = Session["filteredAddress"];
            gridAddressBook.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SortedDictionary<string, string> result = null;

            if (_phoneNumber != "")
                result = new SortedDictionary<string, string>(AddressDictionary.Instance.AddrDict.Where(item => item.Value == _phoneNumber).ToDictionary(kvp => kvp.Key, kvp => kvp.Value));

            if (_name != "")
                if (result != null)
                    result = new SortedDictionary<string, string>(result.Where(item => item.Key.Contains(_name)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value));
                else
                {
                    result = new  SortedDictionary<string, string>(AddressDictionary.Instance.AddrDict.Where(item => item.Key.Contains(_name)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value));
                }

            if (result != null)
            {
                gridAddressBook.DataSource = result;
                gridAddressBook.DataBind();

                lblResult.Text = "You found " + result.Count + (result.Count == 1 ? " address!" : " addresses!");
                lblResult.ForeColor = System.Drawing.ColorTranslator.FromHtml("#009933");

                Session["filteredAddress"] = result;
            }
            else
            {
                lblResult.Text = "You must search for something!";
                lblResult.ForeColor = System.Drawing.Color.Red;
            }

        }
    }
}