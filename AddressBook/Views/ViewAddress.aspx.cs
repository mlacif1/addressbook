﻿using AddressBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace AddressBook
{
    public partial class ViewAddress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            gridAddressBook.DataSource = AddressDictionary.Instance.AddrDict;
            gridAddressBook.DataBind();
        }

        protected void gridAddressBook_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridAddressBook.PageIndex = e.NewPageIndex;
            gridAddressBook.DataSource = AddressDictionary.Instance.AddrDict;
            gridAddressBook.DataBind();
        }
    }
}