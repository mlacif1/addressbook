﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAddress.aspx.cs" Inherits="AddressBook.ViewAddress" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Adress Book</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../Styles/ViewAddressPageStyle.css" />

</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Address Book</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="InsertAddress.aspx">Insert Address</a></li>
                    <li class="active"><a href="#">View Address Book</a></li>
                    <li><a href="SearchAddress.aspx">Search Address</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </nav>

        <div class="container">
            <div id="gridContainer" class="table-responsive">
                <asp:GridView class="table" ID="gridAddressBook" runat="server" AllowPaging="true"  OnPageIndexChanging="gridAddressBook_PageIndexChanging">
                    <HeaderStyle BackColor="#337ab7" ForeColor="White"></HeaderStyle>

                    <PagerSettings Mode="Numeric" Position="Bottom"
                        PageButtonCount="10" />

                    <PagerStyle BackColor="#337ab7"
                        Height="30px"
                        ForeColor="White"
                        VerticalAlign="Bottom"
                        HorizontalAlign="Center" />

                </asp:GridView>
            </div>
        </div>
    </form>
</body>
</html>
