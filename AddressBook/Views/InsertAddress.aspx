﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertAddress.aspx.cs" Inherits="AddressBook.InsertAddress" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Insert Address</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="../Scripts/InsertAddressScript.js"></script>

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" />

    <link rel="stylesheet" href="../Styles/InsertAddressPageStyle.css" />

</head>
<body>
    <form runat="server">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Address Book</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#">Insert Address</a></li>
                    <li><a href="ViewAddress.aspx">View Address Book</a></li>
                    <li><a href="SearchAddress.aspx">Search Address</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </nav>

        <div class="container">

            <div id="inputNumberGenerator">
                <div class="input-group spinner">
                    <asp:TextBox CssClass="form-control" Text="1" ID="txtSpinner" runat="server"></asp:TextBox>
                    <div class="input-group-btn-vertical">
                        <button class="btn btn-default" type="button"><i class="fa fa-caret-up" runat="server"></i></button>
                        <button class="btn btn-default" type="button"><i class="fa fa-caret-down" runat="server"></i></button>
                    </div>
                </div>
                <asp:Button ID="btnGenerate" runat="server" Text="Add More Addresses" CssClass="btn-secondary" OnClick="btnGenerate_Click" />

            </div>

            <br />

            <div id="inputsContainer">
                <asp:Panel ID="aspInputsContainer" runat="server">
                </asp:Panel>
            </div>

            <br />

            <asp:Label ID="lblResult" runat="server"></asp:Label>
            <asp:Button ID="btnInsert" runat="server" Text="Insert Address" CssClass="btn-primary" OnClick="btnInsert_Click" />

        </div>
    </form>

</body>
</html>
