﻿$(document).ready(function () {
    (function ($) {
        $('.spinner .btn:first-of-type').on('click', function () {
            var newValue = parseInt($('.spinner input').val(), 10) + 1;
            $('.spinner input').val(newValue > 50 ? 50 : newValue);
        });
        $('.spinner .btn:last-of-type').on('click', function () {
            var newValue = parseInt($('.spinner input').val(), 10) - 1;
            $('.spinner input').val(newValue < 1 ? 1 : newValue);
        });
    })(jQuery);

});
